/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Windows10
 */
public class ReceiptDetail {
    private int id;
    private Product product;
    private int amount;
    private double price;
    private Receipt recceipt;

    public ReceiptDetail(int id, Product product, int amount, double price, Receipt recceipt) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.recceipt = recceipt;
    }

    public ReceiptDetail(Product product, int amount, double price, Receipt recceipt) {
        this(-1, product, amount, price, recceipt);
    }
    
    public double getTotal(){
        return amount * price;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getRecceipt() {
        return recceipt;
    }

    public void setRecceipt(Receipt recceipt) {
        this.recceipt = recceipt;
    }

    public void addAmount(int amount) {
        this.amount = this.amount + amount;
    }
    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id 
                + ", product=" + product 
                + ", amount=" + amount 
                + ", price=" + price
                + ", total=" + this.getTotal()
                + '}';
    }
    
}
